import 'package:flutter/material.dart';

class ColorsApp {
  Color buttonFacebook = Colors.blueAccent;
  Color buttonGoogle = Colors.blue;
  Color buttonDefault = Colors.grey;
  Color buttonAdd = Colors.red;
  Color buttonTransparent = Colors.transparent;
}
