import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:projeto_soma/Screens/Login/Login_Bloc.dart';
import 'package:projeto_soma/Validators/Validators.dart';

class LoginComponents {
  static String email;
  static String password;

  iconLogin() {
    return ListTile(
      title: Icon(Icons.account_circle, size: 120),
    );
  }

//comentario
  inputEmail() {
    return Column(
      children: <Widget>[
        TextFormField(
          autofocus: true,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: "Email",
            hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
            border: OutlineInputBorder(),
          ),
          validator: (String value) {
            return Validators().validacaoEmail(value);
          },
          onChanged: (String value) {
            email = value;
          },
        ),
        SizedBox(height: 20)
      ],
    );
  }

  buttonSigIn() {
    return ButtonTheme(
      height: 50.0,
      child: RaisedButton(
        color: Colors.grey,
        onPressed: () {
          LoginBloc().getLogin(email, password);
        },
        child: Text(
          "ENTRAR",
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      ),
    );
  }

  inputPassword() {
    return TextFormField(
      autofocus: true,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: "Senha",
        hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
        border: OutlineInputBorder(),
      ),
      validator: (String value) {
        if (value.isEmpty || value.toString().length < 8) {
          return 'Insira uma senha de no minimo 8 digitos';
        } else {
          return null;
        }
      },
      onChanged: (String value) {
        password = value;
      },
    );
  }

  buttonSigInFacebook() {
    return ButtonTheme(
      height: 50.0,
      child: RaisedButton(
        color: Colors.blue,
        onPressed: () {
          print("pressionei o botão");
        },
        child: ListTile(
          leading: Icon(FontAwesomeIcons.facebookF, color: Colors.white),
          title: Text(
            "ENTRAR COM FACEBOOK",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }

  buttonSigInGoogle() {
    return ButtonTheme(
      height: 50.0,
      child: RaisedButton(
        color: Colors.lightBlue,
        onPressed: () {
          print("pressionei o botão");
        },
        child: ListTile(
          leading: Icon(FontAwesomeIcons.google, color: Colors.white),
          title: Text(
            "ENTRAR COM GOOGLE",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }

  buttonSignUp() {
    return ButtonTheme(
      height: 50.0,
      child: RaisedButton(
        color: Colors.grey,
        onPressed: () {
          print("pressionei o botão");
        },
        child: Text(
          "CADASTRE-SE",
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      ),
    );
  }
}
