import 'package:projeto_soma/Screens/Cadastro/Cadastro_Page.dart';
import 'package:projeto_soma/Screens/Login/Login_Page.dart';

class RotasPaginas {
  rotasAppGeral() {
    dynamic rotas = {
      //Rota Inicial do APP;
      '/': (context) => LoginPage(),

      //Rotas para ser chamadas;
      '/cadastro': (context) => CadastroPage(),
      '/login': (context) => LoginPage(),
    };
    return rotas;
  }
}
