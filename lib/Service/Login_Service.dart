import 'package:projeto_soma/Service/Abstract_Service.dart';

class LoginService extends AbstractService {
  LoginService() : super('/login');

  Future<dynamic> getUsuario(String email, String senha) =>
      Session.get('$api/email=$email&senha=$senha=').then((json) {
        print(json);
        return json.length == null ? null : fromJson(json);
      });

  @override
  fromJson(json) {
    return null;
  }
}
