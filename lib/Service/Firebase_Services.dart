import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:projeto_soma/Model/Usuario_Model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:projeto_soma/Service/Abstract_Service.dart';

class FirebaseServices {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  Usuario user;

  //login senha
  Future<Usuario> handleEmailPasswordSignUp(
      String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return user;
    } catch (e) {
      // TRATAR ERRO
      return null;
    }
  }

  //login facebook
  Future<FirebaseUser> signInWithFacebook() async {
    try {
      var fb = FacebookLogin();
      fb.loginBehavior = FacebookLoginBehavior.webViewOnly;
      final FacebookLoginResult result =
          await fb.logInWithReadPermissions(['email']);
      print('!!!!!!!!!!!!!!');
      print(result.errorMessage);
      print(result.accessToken.token);

      final AuthCredential credential = FacebookAuthProvider.getCredential(
          accessToken: result.accessToken.token);

      FirebaseUser user = await _auth.signInWithCredential(credential);
      //await user.updateEmail(profile['email']);
      print(user.displayName);
      print(user.email);
      var token = await user.getIdToken();
      Session.headers['authorization'] = 'Bearer $token';
      return user;
    } catch (e) {
      e.message;
      //  changePersonCheck(e.code);
      return null;
    }
  }

  Future<Null> logout() async {
    await _auth.signOut();
  }
}
