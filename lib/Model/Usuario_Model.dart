class Usuario {
  int id;
  String nome;
  String sexo;
  String dataNascimento;
  String uuid;
  String tokenTelefone;
  String email;
  String foto;
  String senha;
  bool firtsLogin;
  int tipoUsuarioId;

  Usuario();

  factory Usuario.fromJson(json) {
    return new Usuario()
      ..id = json['id']
      ..nome = json['nome']
      ..sexo = json['sexo']
      ..dataNascimento = (json['datanascimento'])
      ..uuid = json['UID']
      ..tokenTelefone = json['UID_phone']
      ..foto = json['foto']
      ..email = json['email']
      ..tipoUsuarioId = json['tipousuario_id']
      ..senha = json['senha'];
  }

  Map toJson() {
    return {
      'id': id,
      'nome': nome,
      'sexo': sexo,
      'datanascimento': dataNascimento,
      'UID': uuid,
      'UID_phone': tokenTelefone,
      'email': email,
      'foto': foto,
      'tipousuario_id': tipoUsuarioId,
      'senha': senha
    };
  }
}
