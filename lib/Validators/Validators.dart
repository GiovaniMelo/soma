import 'package:projeto_soma/Screens/Login/Login_Redux.dart';

class Validators {
  String erro;
  final LoginRedux loginRedux = new LoginRedux();
  validacaoEmail(String valor) {
    if (valor.isEmpty ||
        !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            .hasMatch(valor)) {
      return 'Por favor, insira um e-mail válido';
    }
    return null;
  }
}
