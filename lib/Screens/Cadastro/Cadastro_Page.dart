import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:projeto_soma/Screens/Login/Login_Redux.dart';
import 'package:projeto_soma/Validators/Validators.dart';
import 'package:projeto_soma/helpers/navegacao.dart';

class CadastroPage extends StatefulWidget {
  @override
  _CadastroPageState createState() => _CadastroPageState();
}

class _CadastroPageState extends State<CadastroPage> {
  //valida os campos se estão OK
  final _formKey = GlobalKey<FormState>();
  LoginRedux _loginRedux = LoginRedux();

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ListTile(
                      title: Icon(Icons.account_circle, size: 120),
                    ),
                    SizedBox(height: 40),
                    formLogin(),
                    SizedBox(height: 70),
                    //BUTTON LOGIN
                    StreamBuilder(
                        stream: _loginRedux.validacao,
                        builder: (context, snapshot) {
                          if (snapshot.data == true) {}
                          return ButtonTheme(
                            height: 50.0,
                            child: RaisedButton(
                              color: Colors.grey,
                              onPressed: () {
                                {
                                  Navigator.pushNamed(context, '/login');
                                }
                              },
                              child: Text(
                                "SAIR",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontFamily: 'Roboto Condensed'),
                              ),
                            ),
                          );
                        }),
                    SizedBox(height: 20),
                    //BUTTON FACEBOOK
                    ButtonTheme(
                      height: 50.0,
                      child: RaisedButton(
                        color: Colors.blue,
                        onPressed: () {
                          print("pressionei o botão");
                        },
                        child: ListTile(
                          leading: Icon(FontAwesomeIcons.facebookF,
                              color: Colors.white),
                          title: Text(
                            "ENTRAR COM FACEBOOK",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    //BUTTON GOOGLE
                    ButtonTheme(
                      height: 50.0,
                      child: RaisedButton(
                        color: Colors.lightBlue,
                        onPressed: () {
                          print("pressionei o botão");
                        },
                        child: ListTile(
                          leading: Icon(FontAwesomeIcons.google,
                              color: Colors.white),
                          title: Text(
                            "ENTRAR COM GOOGLE",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 74),
                    //BUTTON CADASTRE-SE
                    ButtonTheme(
                      height: 50.0,
                      child: RaisedButton(
                        color: Colors.grey,
                        onPressed: () {
                          print("pressionei o botão");
                        },
                        child: Text(
                          "CADASTRE-SE",
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  formLogin() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            autofocus: true,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: "Email",
              hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
              border: OutlineInputBorder(),
            ),
            validator: (String value) {
              return Validators().validacaoEmail(value);
            },
            onSaved: (String value) {
              //  LoginBloc().changeInputEmail(value);
            },
          ),
          SizedBox(height: 20),
          TextFormField(
            autofocus: true,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: "Senha",
              hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
              border: OutlineInputBorder(),
            ),
            validator: (String value) {
              if (value.isEmpty || value.toString().length < 8) {
                return 'Insira uma senha de no minimo 8 digitos';
              } else {
                return null;
              }
            },
          ),
        ],
      ),
    );
  }

  errorUsuario(String retornoMetodo) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Usuario não encontrado"),
          content: Text(retornoMetodo),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                pop(context);
              },
              child: Text("Ok"),
            )
          ],
        );
      },
    );
  }
}
