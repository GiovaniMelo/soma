import 'package:firebase_auth/firebase_auth.dart';
import 'package:projeto_soma/Model/Usuario_Model.dart';
import 'package:projeto_soma/Service/Firebase_Services.dart';

import 'Login_Redux.dart';

class LoginBloc {
  FirebaseServices _firebaseServices = new FirebaseServices();
  getLogin(String email, String password) async {
    try {
      print(email);

      await FirebaseServices().handleEmailPasswordSignUp(email, password);

      LoginRedux().changeValidacao(true);
    } catch (e) {
      //Tratamento de erros;

    }
  }

  Future<FirebaseUser> signInFacebook() async {
    return await _firebaseServices.signInWithFacebook();
  }
}
