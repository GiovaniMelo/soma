import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:projeto_soma/Model/Usuario_Model.dart';
import 'package:projeto_soma/Screens/Login/Login_Bloc.dart';
import 'package:projeto_soma/Screens/Login/Login_Redux.dart';
import 'package:projeto_soma/components/buttonThemeGeral.dart';
import 'package:projeto_soma/components/colorsApp.dart';
import 'package:projeto_soma/components/inputEmailText.dart';
import 'package:projeto_soma/components/inputPasswordText.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final LoginRedux loginRedux = new LoginRedux();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final LoginBloc _loginBloc = new LoginBloc();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  //valida os campos se estão OK
  final _formKey = GlobalKey<FormState>();
  static final FacebookLogin facebookSignIn = new FacebookLogin();

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 50),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.account_circle, size: 120),
                SizedBox(height: 40),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      InputEmailText(
                        hintText: 'Email',
                        controller: emailController,
                      ),
                      SizedBox(height: 20),
                      InputPasswordText(
                        hintText: 'Senha',
                        controller: passwordController,
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      'Esqueceu sua senha?',
                      style: TextStyle(color: Colors.red, fontSize: 12),
                    )
                  ],
                ),
                SizedBox(height: 70),
                ButtonThemeGeral(
                  color: ColorsApp().buttonAdd,
                  title: Center(
                    child: Text('Entrar',
                        style: TextStyle(color: Colors.white, fontSize: 16)),
                  ),
                  onPressed: () {
                    // VERIFICA SE CAMPOS ESTÃO PREENCHIDOS
                    if (_formKey.currentState.validate()) {}
                  },
                ),
                SizedBox(height: 20),
                ButtonThemeGeral(
                  leading:
                      Icon(FontAwesomeIcons.facebookF, color: Colors.white),
                  color: ColorsApp().buttonFacebook,
                  title: Center(
                    child: Text('Entrar com Facebook',
                        style: TextStyle(color: Colors.white, fontSize: 16)),
                  ),
                  onPressed: () async {
                    FirebaseUser user = await _loginBloc.signInFacebook();
                    print(user);
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                ButtonThemeGeral(
                  leading: Icon(FontAwesomeIcons.google, color: Colors.white),
                  color: ColorsApp().buttonGoogle,
                  title: Center(
                    child: Text('Entrar com Google',
                        style: TextStyle(color: Colors.white, fontSize: 16)),
                  ),
                  onPressed: () {},
                ),
                SizedBox(height: 74),
                Container(
                  decoration: new BoxDecoration(
                    border: new Border.all(
                        color: ColorsApp().buttonAdd, width: 2.0),
                    borderRadius: new BorderRadius.circular(0),
                  ),
                  child: ButtonThemeGeral(
                    color: Colors.yellow,
                    title: Center(
                      child: Text('Cadastre-se Agora',
                          style: TextStyle(color: Colors.red, fontSize: 16)),
                    ),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
