import 'package:rxdart/rxdart.dart';

class LoginRedux {
  final _validacao = BehaviorSubject<bool>();
  final _messageError = BehaviorSubject<String>();

  Stream<bool> get validacao => _validacao.stream;
  Stream<String> get messageError => _messageError.stream;

  Function(bool) get changeValidacao => _validacao.sink.add;
  Function(String) get changeMessageError => _messageError.sink.add;

  void dispose() {
    _validacao.close();
    _messageError.close();
  }
}
